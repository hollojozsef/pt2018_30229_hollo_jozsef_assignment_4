package PT2018.Tema4;



import org.junit.Test;

import junit.framework.TestCase;

public class AppTest extends TestCase {
	int suma=5;
	Person p1=new Person(1,"Vasile");
	Person p2=new Person(2,"Petre");
	SavingAccount a1=new SavingAccount(1,1,100);
	SavingAccount a2=new SavingAccount(2,2,100);
	SpendingAccount a3=new SpendingAccount(3,1,200);
	SpendingAccount a4=new SpendingAccount(4,2,300);
	@Test
	public void testretragere() {
		a1.retragere();
		assertEquals(a1.getSum(),0);
	}
	@Test
	public void testadaugare() {
		a2.adaugare(suma);
		assertEquals(a2.getSum(),115);
	}
	@Test
	public void testadaugareSpending() {
		a3.adaugare(suma);
		assertEquals(a3.getSum(),205);
	}
	@Test
	public void testretragereSpending() {
		a4.retragere(suma);
		assertEquals(a4.getSum(),295);
	}
}
