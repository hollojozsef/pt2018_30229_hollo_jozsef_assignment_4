package PT2018.Tema4;

public class SavingAccountObserver extends Observator{
	private SavingAccount person;

	public SavingAccountObserver(SavingAccount person){
	      this.person = person;
	      this.person.attach(this);
	   }
	@Override
	   public void update() {
	      System.out.println( "Saving account modified"  ); 
	   }
}
