package PT2018.Tema4;

import java.util.HashSet;

public interface BankProc {
	/**
	 * @pre 
	 * @post banca.size()==banca.size()@pre+1
	 * @param p
	 */
	public void addPerson(Person p);
	/**
	 * pre
	 * post banca.get(p).size()=banca.get(p).size()@pre+1
	 * @param p
	 * @param a
	 */
	public void addAccount(Person p, Account a);
	/**
	 * @pre
	 * @post banca.size()=banca.size()@pre-1
	 * @param p
	 */
	public void deletePerson(Person p);
	/**
	 * @pre
	 * @post banca.get(p).size()=banca.get(p).size@pre-1
	 * @param p
	 * @param a
	 */
	public void deleteAccount(Person p, Account a);
	/**
	 * @pre p!=NULL
	 * @post banca.get(p).getPersonName!=banca.get(p).getPersonName@pre
	 * @param p
	 * @param nume
	 */
	public void editPerson(Person p, String nume);
	/**
	 * @pre	(a!=NULL)&&(p!=NULL)
	 * @post banca.get(p).equals(a)=false
	 * @param p
	 * @param a
	 * @param suma
	 */
	public void editAccount(Person p, Account a, int suma);
	/**
	 * @pre(p!=NULL)&&(a!=NULL)
	 * @post banca.get(p).get(a).getSum>banca.get(a).get(p).getSum@pre
	 * @param p
	 * @param a
	 * @param sumaBani
	 */
	public void addSaving(Person p, SavingAccount a, int sumaBani);
	/**
	 * @pre(p!=NULL)&&(a!=NULL)
	 * @post banca.get(p).get(a).getSum==0
	 * @param p
	 * @param a
	 */
	public void retragereSaving(Person p, SavingAccount a);
	/**
	 * @pre(p!=NULL)&&(a!=NULL)
	 * @post banca.get(p).get(a).getSum>banca.get(p).get(a).getSum@pre
	 * @param p
	 * @param a
	 * @param sumaBani
	 */
	public void addSpending(Person p, SpendingAccount a, int sumaBani);
	/**
	 * @pre(p!=NULL)&&(a!=NULL)
	 * @post banca.get(p).get(a).getSum<banca.get(p).get(a).getSum@pre
	 * @param p
	 * @param a
	 * @param sumaBani
	 */
	public void retragereSpending(Person p, SpendingAccount a, int sumaBani);
}
