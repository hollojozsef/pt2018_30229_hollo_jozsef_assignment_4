package PT2018.Tema4;

import java.util.ArrayList;
import java.util.List;
import java.util.Observer;

public class SavingAccount extends Account{
	private List<Observator> observers = new ArrayList<Observator>();
	public SavingAccount(int accountID, int holderID, int sum) {
		super(accountID, holderID, sum);
		// TODO Auto-generated constructor stub
	}
	public void retragere() {
		this.setSum(0);
		notifyAllObservers();
	}
	public void adaugare(int suma) {
		this.setSum((this.getSum()+suma)+((this.getSum()+suma)/10));
		notifyAllObservers();
	}
	public void attach(Observator observer) {
		observers.add(observer);
	}

	public void notifyAllObservers() {
		for (Observator observer : observers) {
			observer.update();
		}
	}
}
