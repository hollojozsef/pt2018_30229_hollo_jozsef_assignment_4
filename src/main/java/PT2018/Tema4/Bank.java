package PT2018.Tema4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Bank {
	HashMap<Person, HashSet<Account>> banca = new HashMap<Person, HashSet<Account>>();
	public HashMap<Person, HashSet<Account>> getBanca() {
		return banca;
	}

public void setBanca(HashMap<Person, HashSet<Account>> banca) {
		this.banca = banca;
	}
	
	public void addPerson(Person p) {
		int sizePre=banca.size();
		banca.put(p, new HashSet<Account>());
		int sizePost=banca.size();
		assert sizePost==sizePre+1:"Adaugare esuata";
	
	}

	public void addAccount(Person p, Account a) {
		int sizePre;
		if(banca.get(p).isEmpty()==true)
			sizePre=0;
		else
			sizePre=banca.get(p).size();
		banca.get(p).add(a);
		int sizePost=banca.get(p).size();
		assert sizePost==sizePre+1:"Adaugare cont esuata";
	}

	public void deletePerson(Person p) {
		int sizePre=banca.size();
		banca.remove(p);
		int sizePost=banca.size();
		assert sizePost==sizePre-1:"Stergere esuata";
	}

	public void deleteAccount(Person p, Account a) {
		int sizePre=banca.get(p).size();
		//System.out.println(sizePre);
		banca.get(p).remove(a);
		int sizePost=banca.get(p).size();
		//System.out.println(sizePost);
		assert sizePost==sizePre-1:"Stergere cont esuata";
	}

	public void editPerson(Person p, String nume) {
	
		Person persoanaInitiala=p;
		HashSet<Account> p2 = banca.get(p);
		banca.remove(p);
		p.setPersonName(nume);
		Person persoanaFinal=p;
		banca.put(p, p2);
		assert banca.get(persoanaInitiala).equals(persoanaFinal)==false:"Editare persoana esuata";
	
		
	}

	public void editAccount(Person p, Account a, int suma) {
		Account contInitial=a;
		Account b = new Account(a.getAccountID(), a.getHolderID(), suma);
		banca.get(p).remove(a);
		banca.get(p).add(b);
		assert banca.get(p).equals(contInitial)==false:"Editare cont esuata";
	}

	public void addSaving(Person p, SavingAccount a, int sumaBani) {
		int sumaInitiala=a.getSum();
		banca.get(p).remove(a);
		a.adaugare(sumaBani);
		int sumaFinala=a.getSum();
		banca.get(p).add(a);
		assert sumaFinala==sumaInitiala+sumaBani:"Adaugare SavingAccount esuata";
	}

	public void retragereSaving(Person p, SavingAccount a) {
		banca.get(p).remove(a);
		a.retragere();
		int sumaFinala=a.getSum();
		banca.get(p).add(a);
		assert sumaFinala==0:"Retragere SavingAccount esuata";
	}

	public void addSpending(Person p, SpendingAccount a, int sumaBani) {
		int sumaInitiala=a.getSum();
		banca.get(p).remove(a);
		a.adaugare(sumaBani);
		int sumaFinala=a.getSum();
		banca.get(p).add(a);
		assert sumaFinala>sumaInitiala:"Adaugare SpendingAccount esuata";
	}

	public void retragereSpending(Person p, SpendingAccount a, int sumaBani) {
		int sumaInitiala=a.getSum();
		banca.get(p).remove(a);
		a.retragere(sumaBani);
		int sumaFinala=a.getSum();
		banca.get(p).add(a);
		assert sumaFinala<sumaInitiala:"Retragere SpendingAccount esuata";
	}

	@Override
	public String toString() {
		Set<Person> ps = banca.keySet();
		for (Person p : ps) {
			System.out.println(p.toString());
			System.out.println(banca.get(p).toString());
		}
		return null;
	}

}