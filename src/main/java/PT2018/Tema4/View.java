package PT2018.Tema4;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class View {

	public View(Bank b) {
		afisareTabele(b);

	}

	public void afisareTabele(Bank b) {

		JFrame frame = new JFrame("Persoane");
		JPanel panel = new JPanel();
		JFrame frame2 = new JFrame("Conturi");
		JPanel panel2 = new JPanel();
		frame.setSize(500, 400);
		frame.setVisible(true);
		frame.add(panel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame2.setSize(500, 400);
		frame2.setVisible(true);
		frame2.add(panel2);
		frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		String col[] = { "ID", "Nume" };
		String col2[] = { "IDHolder", "IDCont", "Nume" };
		DefaultTableModel tableModel = new DefaultTableModel(col, 0);
		DefaultTableModel tableModel2 = new DefaultTableModel(col2, 0);
		JTable table = new JTable(tableModel);
		JTable table2 = new JTable(tableModel2);
		table.setFillsViewportHeight(true);
		table.setVisible(true);
		table2.setFillsViewportHeight(true);
		table2.setVisible(true);
		frame.add(new JScrollPane(table));
		frame2.add(new JScrollPane(table2));
		frame.setVisible(true);
		frame2.setVisible(true);
		Set<Entry<Person, HashSet<Account>>> has = b.getBanca().entrySet();
		Iterator<Entry<Person, HashSet<Account>>> it = b.getBanca().entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry me = (Map.Entry) it.next();
			// System.out.println("Key is"+me.getKey()+"value is"+ me.getValue()+"\n");
			Person per = (Person) me.getKey();
			HashSet<Account> a = (HashSet<Account>) me.getValue();
			Object[] data = { per.getPersonID(), per.getPersonName() };
			tableModel.addRow(data);
			Iterator<Account> it2 = a.iterator();
			while (it2.hasNext()) {
				Account cont = it2.next();
				int HolderIDi = cont.getHolderID();
				int contID = cont.getAccountID();
				int banile = cont.getSum();
				Object[] obje = { HolderIDi, contID, banile };
				tableModel2.addRow(obje);

			}
		}

	}
}
