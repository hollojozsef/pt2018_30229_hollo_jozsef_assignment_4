package PT2018.Tema4;

import java.util.ArrayList;
import java.util.List;
import java.util.Observer;

public class Person {
	public int personID;
	public String personName;
	

	public Person(int personID, String personName) {
		this.personID = personID;
		this.personName = personName;
		
	}

	public int getPersonID() {
		return personID;
	}

	public void setPersonID(int personID) {
		this.personID = personID;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	@Override
	public boolean equals(Object o) {
		if ((o instanceof Person) && (((Person) o).getPersonID() == this.personID)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		int result = 0;
		result = (int) (personID % 1997);
		return result;
	}

	@Override
	public String toString() {
		return "personID=" + personID + ", personName=" + personName;
	}




}
