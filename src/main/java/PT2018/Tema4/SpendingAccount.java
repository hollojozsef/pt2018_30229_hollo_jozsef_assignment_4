package PT2018.Tema4;

import java.util.ArrayList;
import java.util.List;
import java.util.Observer;

public class SpendingAccount extends Account{
	private List<Observator> observers = new ArrayList<Observator>();
	public SpendingAccount(int accountID, int holderID, int sum) {
		super(accountID, holderID, sum);
		// TODO Auto-generated constructor stub
	}
	public void retragere(int suma) {
		//System.out.println(this.getSum());
		int a=this.getSum();
		this.setSum(a-suma);
		//System.out.println(this.getSum());
		notifyAllObservers();
	}
	public void adaugare(int suma) {
		this.setSum(getSum()+suma);
		notifyAllObservers();
	}
	public void attach(Observator observer) {
		observers.add(observer);
	}

	public void notifyAllObservers() {
		for (Observator observer : observers) {
			observer.update();
		}
	}
}
