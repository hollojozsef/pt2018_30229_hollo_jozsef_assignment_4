package PT2018.Tema4;

import java.awt.EventQueue;

import javax.swing.JFrame;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.awt.event.ActionEvent;

public class ViewPrincipal {
	Bank b=new Bank();
	JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	Observator ob;
	

	public ViewPrincipal() {
		

	

		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(10, 38, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(148, 38, 86, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblIdPersoana = new JLabel("ID Persoana");
		lblIdPersoana.setBounds(10, 24, 86, 14);
		frame.getContentPane().add(lblIdPersoana);
		
		JLabel lblNume = new JLabel("Nume");
		lblNume.setBounds(150, 24, 46, 14);
		frame.getContentPane().add(lblNume);
		
		JButton btnAdaugarePersoana = new JButton("Adaugare Persoana");
		btnAdaugarePersoana.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int s1=Integer.parseInt(textField.getText());
				String s2=textField_1.getText();
				Person p1=new Person(s1,s2);
				b.addPerson(p1);
				//System.out.println("ID:  "+p1.getPersonID()+" hash "+p1.hashCode());
			}
		});
		btnAdaugarePersoana.setBounds(267, 11, 157, 23);
		frame.getContentPane().add(btnAdaugarePersoana);
		
		JButton btnStergerePersoana = new JButton("Stergere Persoana");
		btnStergerePersoana.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int s1=Integer.parseInt(textField.getText());
				String s2=textField_1.getText();
				Person pers=new Person(s1,s2);
				b.deletePerson(pers);
				
			}
		});
		btnStergerePersoana.setBounds(267, 37, 157, 23);
		frame.getContentPane().add(btnStergerePersoana);
		
		JButton btnEditarePersoana = new JButton("Editare Persoana");
		btnEditarePersoana.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int s1=Integer.parseInt(textField.getText());
				String s2=textField_1.getText();
				String s3=textField_5.getText();
				Person p1=new Person(s1,s2);
				b.editPerson(p1,s3);
			}
		});
		btnEditarePersoana.setBounds(267, 64, 157, 23);
		frame.getContentPane().add(btnEditarePersoana);
		
		textField_2 = new JTextField();
		textField_2.setBounds(10, 169, 86, 20);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(106, 169, 86, 20);
		frame.getContentPane().add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(202, 169, 86, 20);
		frame.getContentPane().add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblIdCont = new JLabel("ID Cont");
		lblIdCont.setBounds(10, 157, 46, 14);
		frame.getContentPane().add(lblIdCont);
		
		JLabel lblIdTitular = new JLabel("ID Titular");
		lblIdTitular.setBounds(106, 157, 46, 14);
		frame.getContentPane().add(lblIdTitular);
		
		JButton btnAdaugareCont = new JButton("Adaugare Cont");
		btnAdaugareCont.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int s1=Integer.parseInt(textField_2.getText());
				int s2=Integer.parseInt(textField_3.getText());
				int s3=Integer.parseInt(textField_4.getText());
				int s5=Integer.parseInt(textField.getText());
				String s4=textField_1.getText();
				Person per=new Person(s5,s4);
				Account saving=new Account(s1,s2,s3);
				b.addAccount(per, saving);		
			}
		});
		btnAdaugareCont.setBounds(298, 136, 126, 23);
		frame.getContentPane().add(btnAdaugareCont);
		
		JButton btnStergereCont = new JButton("Stergere Cont");
		btnStergereCont.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int s1=Integer.parseInt(textField_2.getText());
				int s2=Integer.parseInt(textField_3.getText());
				int s3=Integer.parseInt(textField_4.getText());
				int s5=Integer.parseInt(textField.getText());
				String s4=textField_1.getText();
				Person per=new Person(s2,s4);
				Account saving=new Account(s1,s2,s3);
				b.deleteAccount(per, saving);		
			}
		});
		btnStergereCont.setBounds(298, 168, 126, 23);
		frame.getContentPane().add(btnStergereCont);
		
		JButton btnDepunere = new JButton("Depunere");
		btnDepunere.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int s1=Integer.parseInt(textField_2.getText());
				int s2=Integer.parseInt(textField_3.getText());
				int s3=Integer.parseInt(textField_4.getText());
				int s6=Integer.parseInt(textField_6.getText());
				int s7=Integer.parseInt(textField_7.getText());
				String s4=textField_1.getText();
				Person per=new Person(s2,s4);
				
				if(s6==1) {//saving
					Account saving=new SavingAccount(s1,s2,s3);
					ob=new SavingAccountObserver((SavingAccount) saving);

					b.deleteAccount(per, saving);
					((SavingAccount)saving).adaugare(s7);
					b.addAccount(per, saving);
				}else {
					Account spending=new SpendingAccount(s1,s2,s3);
					b.deleteAccount(per, spending);
					((SpendingAccount)spending).adaugare(s7);
					b.addAccount(per, spending);
				}
			}
		});
		btnDepunere.setBounds(321, 202, 103, 23);
		frame.getContentPane().add(btnDepunere);
		
		JButton btnRetragere = new JButton("Retragere");
		btnRetragere.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int s1=Integer.parseInt(textField_2.getText());
				int s2=Integer.parseInt(textField_3.getText());
				int s3=Integer.parseInt(textField_4.getText());
				int s6=Integer.parseInt(textField_6.getText());
				int s7=Integer.parseInt(textField_7.getText());
				String s4=textField_1.getText();
				Person per=new Person(s2,s4);
				
				if(s6==1) {//saving
					Account saving=new SavingAccount(s1,s2,s3);
					b.deleteAccount(per, saving);
					((SavingAccount)saving).retragere();
					b.addAccount(per, saving);
				}else {
					Account spending=new SpendingAccount(s1,s2,s3);
					b.deleteAccount(per, spending);
					((SpendingAccount)spending).retragere(s7);
					b.addAccount(per, spending);
				}
			}
		});
		btnRetragere.setBounds(321, 236, 103, 23);
		frame.getContentPane().add(btnRetragere);
		
		JButton btnAfisareTabele = new JButton("Afisare Tabele");
		btnAfisareTabele.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				afisareTabele(b);
			}
		});
		btnAfisareTabele.setBounds(19, 213, 133, 37);
		frame.getContentPane().add(btnAfisareTabele);
		
		textField_5 = new JTextField();
		textField_5.setBounds(148, 69, 86, 20);
		frame.getContentPane().add(textField_5);
		textField_5.setColumns(10);
		
		JLabel lblNumeNou = new JLabel("Nume Nou");
		lblNumeNou.setBounds(146, 57, 60, 14);
		frame.getContentPane().add(lblNumeNou);
		
		textField_6 = new JTextField();
		textField_6.setBounds(202, 221, 86, 20);
		frame.getContentPane().add(textField_6);
		textField_6.setColumns(10);
		
		JLabel lblTipCont = new JLabel("Tip Cont");
		lblTipCont.setBounds(202, 206, 46, 14);
		frame.getContentPane().add(lblTipCont);
		
		JLabel lblSumaTotala = new JLabel("Suma Totala");
		lblSumaTotala.setBounds(202, 157, 72, 14);
		frame.getContentPane().add(lblSumaTotala);
		
		textField_7 = new JTextField();
		textField_7.setBounds(202, 138, 86, 20);
		frame.getContentPane().add(textField_7);
		textField_7.setColumns(10);
		
		JLabel lblSuma = new JLabel("Suma");
		lblSuma.setBounds(202, 122, 46, 14);
		frame.getContentPane().add(lblSuma);
	}

		public void afisareTabele(Bank b) {
		
			JFrame frame = new JFrame("Persoane");
			JPanel panel = new JPanel();
			JFrame frame2 = new JFrame("Conturi");
			JPanel panel2 = new JPanel();
			frame.setSize(500, 400);
			frame.setVisible(true);
			frame.getContentPane().add(panel);
			//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			

			frame2.setSize(500, 400);
			frame2.setVisible(true);
			frame2.getContentPane().add(panel2);
			//frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			String col[] = { "ID", "Nume" };
			String col2[] = { "IDHolder", "IDCont", "Suma" };
			DefaultTableModel tableModel = new DefaultTableModel(col, 0);
			DefaultTableModel tableModel2 = new DefaultTableModel(col2, 0);
			JTable table = new JTable(tableModel);
			JTable table2 = new JTable(tableModel2);
			table.setFillsViewportHeight(true);
			table.setVisible(true);
			table2.setFillsViewportHeight(true);
			table2.setVisible(true);
			frame.getContentPane().add(new JScrollPane(table));
			frame2.getContentPane().add(new JScrollPane(table2));
			frame.setVisible(true);
			frame2.setVisible(true);
			Set<Entry<Person, HashSet<Account>>> has = b.getBanca().entrySet();
			Iterator<Entry<Person, HashSet<Account>>> it = b.getBanca().entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry me = (Map.Entry) it.next();
				// System.out.println("Key is"+me.getKey()+"value is"+ me.getValue()+"\n");
				Person per=(Person) me.getKey();
				HashSet<Account> a = (HashSet<Account>) me.getValue();
				Object[] data = { per.getPersonID(), per.getPersonName() };
				tableModel.addRow(data);
				Iterator<Account> it2 = a.iterator();
				while (it2.hasNext()) {
					Account cont = it2.next();
					int HolderIDi = cont.getHolderID();
					int contID = cont.getAccountID();
					int banile = cont.getSum();
					Object[] obje = { HolderIDi, contID, banile };
					tableModel2.addRow(obje);

				}
			}
		
	}
}
