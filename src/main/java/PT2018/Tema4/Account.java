package PT2018.Tema4;

public class Account {
public int accountID;
public int holderID;
public int sum;
public Account(int accountID, int holderID, int sum) {
	this.accountID = accountID;
	this.holderID = holderID;
	this.sum = sum;
}
public int getAccountID() {
	return accountID;
}
public void setAccountID(int accountID) {
	this.accountID = accountID;
}
public int getHolderID() {
	return holderID;
}
public void setHolderID(int holderID) {
	this.holderID = holderID;
}
public int getSum() {
	return sum;
}
public void setSum(int sum) {
	this.sum = sum;
}
@Override
public boolean equals(Object o) {
	if ((o instanceof Account) && (((Account) o).getAccountID() == this.accountID)
			&& ((((Account) o).getHolderID()) == this.holderID)) {
		return true;
	} else {
		return false;
	}
}


@Override
public int hashCode() {
	int result = 0;
	result = accountID % 1997;
	return result;
}
@Override
public String toString() {
	return "accountID=" + accountID + ", holderID=" + holderID + ", sum=" + sum ;
}




}
